package main

import (
	"fmt"
	"os"
)

func fill(f *os.File, b byte, l int64) bool {
	if f == nil {
		return false
	}

	bufSize := 4096

	buf := make([]byte, bufSize)

	for i := range buf {
		buf[i] = b
	}

	f.Seek(0, 0)

	for l > 0 {
		wb := 0

		if l < int64(bufSize) {
			wb = int(l)
		} else {
			wb = bufSize
		}

		n, err := f.Write(buf[0:wb])

		if err != nil {
			return false
		}

		l = l - int64(n)
	}

	return true
}

func clean(path string) {
	f, err := os.OpenFile(path, os.O_RDWR, 0644)

	defer f.Close()

	if err != nil {
		panic(err)
	}

	fi, err := f.Stat()

	if err != nil {
		panic(err)
	}

	fill(f, 0xff, fi.Size())
}

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "usage: %s <file_path>\n", os.Args[0])

		os.Exit(1)
	}

	var path = os.Args[1]

	fi, err := os.Stat(path)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: Failed check file <%s>.\n", err.Error())

		os.Exit(1)
	}

	if fi.IsDir() == true {
		fmt.Fprintf(os.Stderr, "Error: File is directory.\n")

		os.Exit(1)
	}

	if fi.Size() < 1 {
		fmt.Fprintf(os.Stderr, "Error: File is empty.\n")

		os.Exit(1)
	}

	clean(path)
}
